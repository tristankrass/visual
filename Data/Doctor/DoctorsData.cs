﻿using Data.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Doctor
{
    public sealed class DoctorsData : DefinedEntityData
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string IdCode { get; set; }
        public string Certificate { get; set; }
        public DateTime? CertificateDate { get; set; }
    }
}
