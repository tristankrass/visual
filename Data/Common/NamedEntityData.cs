﻿namespace Data.Common
{
    public abstract class NamedEntityData: UniqueEntityData
    {
        public string Email { get; set; }
        public  string PhoneNumber { get; set; }

    }
}