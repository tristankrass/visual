﻿using System.Collections.Generic;
using System.Text;

namespace Data.Common
{
    public abstract class DefinedEntityData : NamedEntityData
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Photo { get; set; }
       
    }
}
