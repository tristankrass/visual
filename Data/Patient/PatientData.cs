﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Data.Common;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Data.Patient
{
    public sealed class PatientData:NamedEntityData
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string IdCode { get; set; }
        public string Gender { get; set; }

        [ForeignKey("UserId")]
        public string UserId { get; set; }
    }
}
