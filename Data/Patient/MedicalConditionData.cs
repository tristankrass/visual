﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Common;

namespace Data.Patient
{
    public sealed class MedicalConditionData :UniqueEntityData
    {
        public string Comment { get; set; }
        public string PatientId { get; set; }
    }
}
