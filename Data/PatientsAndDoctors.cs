﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Common;

namespace Data
{
    public sealed class PatientsAndDoctors:UniqueEntityData
    {
        public string HospitalID { get; set; }
        public string DoctorID { get; set; }
    }
}
