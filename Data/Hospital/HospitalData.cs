﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Data.Common;

namespace Data.Hospital
{
    public sealed class HospitalData : DefinedEntityData
    {
        public string HospitalName { get; set; }

    }
}
