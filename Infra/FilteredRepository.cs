﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Common;
using Domain.Common;
using Domain.Doctor;
using Microsoft.EntityFrameworkCore;

namespace Infra
{
    public abstract class FilteredRepository<TDomain, TData>: SortedRepository<TDomain, TData>, ISearching
        where TData : PeriodData, new()
    where TDomain : Entity<TData>, new()
    {
        public string SearchString { get; set; }
        protected FilteredRepository(DbContext c, DbSet<TData>s): base(c, s) { }
    }

}
