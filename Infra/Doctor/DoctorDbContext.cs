﻿using Data;
using Data.Doctor;
using Data.Hospital;
using Data.Patient;
using Domain.Hospital;
using Facade.Doctor;
using Microsoft.EntityFrameworkCore;

namespace Infra.Doctor
{
    public class DoctorDbContext : DbContext
    {
        public DbSet<DoctorsData> Doctor { get; set; }
        public DbSet<Enrollment> Enrollment { get; set; }
        public DbSet<HospitalData> Hospital{ get; set; }
        public DbSet<PatientData> Patients { get; set; }

        public DbSet<PatientsAndDoctors> PatientsAndDoctors {get; set;}
        public DbSet<MedicalConditionData> MedicalCondition { get; set; }

        public DoctorDbContext(DbContextOptions<DoctorDbContext> options)
            : base(options)
        {
        }
       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            InitializeTables(modelBuilder);
           
            
        }
        public static void InitializeTables(ModelBuilder modelBuilder)
        { 
            modelBuilder.Entity<DoctorsData>().ToTable(nameof(Doctor));
            modelBuilder.Entity<HospitalData>().ToTable(nameof(Hospital));
            modelBuilder.Entity<Enrollment>().ToTable(nameof(Enrollment));
            modelBuilder.Entity<PatientData>().ToTable(nameof(Patients));
            modelBuilder.Entity<PatientsAndDoctors>().ToTable(nameof(PatientsAndDoctors));
        }
    }
}
