﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Doctor;
using Domain;
using Domain.Doctor;
using Microsoft.EntityFrameworkCore;


namespace Infra.Doctor
{
    public class DoctorsRepository : IDoctorRepository
    {
        private readonly DoctorDbContext db;
        private string _pageIndex;
        private string _hasNextPage;

        public string SortOrder { get; set; }
        public string SearchingString { get; set; }

        string IRepository<Domain.Doctor.Doctor>.PageIndex
        {
            get => _pageIndex;
            set => _pageIndex = value;
        }

        string IRepository<Domain.Doctor.Doctor>.HasNextPage
        {
            get => _hasNextPage;
            set => _hasNextPage = value;
        }

        public string HasPrevioysPage { get; set; }
        public string SearchString { get; set; }
        public int PageSize { get; set; } = 1;
        public int PageIndex { get; set; } = 1;
        public bool HasNextPage { get; set; }
        public bool HasPreviousPage { get; set; }

        public DoctorsRepository(DoctorDbContext c)
        {
            db = c;
        }

        public async Task<List<Domain.Doctor.Doctor>> Get()
        {
            var list = await createPaged(createFiltered(createSorted()));

            HasNextPage = list.HasNextPage;
            HasPreviousPage = list.HasPreviousPage;

            return list.Select(e => new Domain.Doctor.Doctor(e)).ToList();
        }
        private async Task<PaginatedList<DoctorsData>> createPaged(IQueryable<DoctorsData> dataSet)
        {
            return await PaginatedList<DoctorsData>.CreateAsync(
                dataSet, PageIndex, PageSize);
        }
        private IQueryable<DoctorsData> createFiltered(IQueryable<DoctorsData> set)
        {
            if (string.IsNullOrEmpty(SearchString)) return set;
            return set.Where(s => s.FirstName.Contains(SearchString)
                                  || s.LastName.Contains(SearchString)
                                  || s.ID.Contains(SearchString)
                                  || s.Email.ToString().Contains(SearchString)
                                  || s.ValidFrom.ToString().Contains(SearchString)
            );
        }
        private IQueryable<DoctorsData> createSorted()
        {
            IQueryable<DoctorsData> doctors = from s in db.Doctor select s;

            switch (SortOrder)
            {
                case "firstname_desc":
                    doctors = doctors.OrderByDescending(s => s.FirstName);
                    break;
                case "lastname_desc":
                    doctors = doctors.OrderByDescending(s => s.LastName);
                    break;
                case "Date":
                    doctors = doctors.OrderBy(s => s.ValidFrom);
                    break;
                default:
                    doctors = doctors.OrderBy(s => s.LastName);
                    break;
            }

            return doctors.AsNoTracking();
        }
        public async Task<Domain.Doctor.Doctor> Get(string id)
        {
            var d = await db.Doctor.FirstOrDefaultAsync(m => m.ID == id);
            return new Domain.Doctor.Doctor(d);
        }

        public async Task Delete(string id)
        {
            if (id is null) return;

            var d = await db.Doctor.FindAsync(id);
            
            if (d != null)
            {
                db.Doctor.Remove(d);
                await db.SaveChangesAsync();
            }
        }
       
        public async Task Add(Domain.Doctor.Doctor obj)
        {
            if (obj?.Data is null) return;
            db.Doctor.Add(obj.Data); 
            await db.SaveChangesAsync();
        }

        public async Task Update(Domain.Doctor.Doctor obj)
        {
            db.Attach(obj.Data).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!MeasureViewExists(MeasureView.Id))
                //{
                //    return NotFound();
                //}
                //else
                //{
                throw;
                //}
            }

        }
    }
}
