﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var fontSize = 100;
$(document).ready(function () {
    let leftEyeResultJson = {};

    let rightEyeResultJson = {
        1: {
            "Correct": "A", "Answer": "",
            // "Options": ["A", "B", "C", "D"]
        },
        2: {
            "Correct": "B", "Answer": "",
            // "Options": ["A", "B", "C", "D"]
        },
        3: {
            "Correct": "B", "Answer": "",
            // "Options": ["A", "B", "C", "D"]
        }
    };
    const comment = "y";


    $(".btn-startTest").on("click", function () {
        $('.letter').css('font-size', fontSize)
        nextSection(".section-1", ".section-2");
    });

    $(".btn-next-1").on("click", function () {
        fontSize -= 15;
        $('.letter').css('font-size', fontSize)
        nextSection(".section-2", ".section-3");
    });
    addAnswers(1, "V", "A", "T", "K");

    function addAnswers(num, letter1, letter2, letter3, letter4) {
        $(`.section-answer-${num}`).on("click", function (e) {
            switch (e.target.id) {
                case `section2-${letter1}`:
                    rightEyeResultJson[num] = letter1;
                    break;
                case `section2-${letter2}`:
                    rightEyeResultJson[num]["Answer"] = letter2;
                    break;
                case `section2-${letter3}`:
                    rightEyeResultJson[num]["Answer"] = letter3;
                    break;
                default:
                    rightEyeResultJson[num]["Answer"] = letter4;
            }
        });
    }


    $(".btn-next-2").on("click", function () {
        fontSize -= 10;
        $('.letter').css('font-size', fontSize)
        nextSection(".section-3", ".section-4");
    });
    addAnswers(2, "7", "L", "E", "W");



    $(".btn-next-3").on("click", function () {
        fontSize -= 10;
        $('.letter').css('font-size', fontSize)
        nextSection(".section-4", ".section-5");
    });


    $(".btn-next-4").on("click", function () {
        fontSize -= 6;
        $('.letter').css('font-size', fontSize)
        nextSection(".section-5", ".section-6");
    });
    // addAnswers(1, "V", "A", "T", "K");

    $(".btn-next-5").on("click", function () {
        nextSection(".section-6", ".section-7");
    });
    // addAnswers(1, "V", "A", "T", "K");


    function nextSection(sectionFrom, sectionTo) {
        $(sectionFrom).removeClass("d-block");
        $(sectionFrom).addClass("d-none");
        $(sectionTo).addClass("d-block");
        $(sectionTo).removeClass("d-none");
    }

    $("#LeftEyeResult").val(JSON.stringify(leftEyeResultJson));
    $("#RightEyeResult").val(JSON.stringify(rightEyeResultJson));
    $("#Comment").val(JSON.stringify(comment));


    $("#submitAnswers").on("click", function () {

        $("#LeftEyeResult").val(JSON.stringify(leftEyeResultJson));
        $("#RightEyeResult").val(JSON.stringify(rightEyeResultJson));
        $("#Comment").val(JSON.stringify(comment));
    });
}); 