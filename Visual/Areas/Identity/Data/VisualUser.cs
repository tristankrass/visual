﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Visual.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the VisualUser class
    public class VisualUser : IdentityUser
    {
    }
}
