using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(Visual.Areas.Identity.IdentityHostingStartup))]
namespace Visual.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}