﻿namespace Visual.Extensions
{
    public static class Constants
    {
        public const string Unspecified = "Unspecified";

        public const string CreateNewLinkTitle = "Create New";
        public const string EditLinkTitle = "Edit";
        public const string DetailsLinkTitle = "Details";
        public const string DeleteLinkTitle = "Delete";

    }
}
