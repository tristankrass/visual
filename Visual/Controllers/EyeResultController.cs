using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Domain.EyeControl;
using Visual.Data;

namespace Visual.Controllers
{
    public class EyeResultController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EyeResultController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: EyeResult
        public async Task<IActionResult> Index()
        {
            return View(await _context.EyeControls.ToListAsync());
        }

        // GET: EyeResult/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eyeControl = await _context.EyeControls
                .FirstOrDefaultAsync(m => m.EyeControlId == id);
            if (eyeControl == null)
            {
                return NotFound();
            }

            return View(eyeControl);
        }

        // GET: EyeResult/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EyeResult/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("EyeControlId,LeftEyeResult,RightEyeResult,Comment,NumberOfTests")]
            EyeControl eyeControl)
        {
            if (ModelState.IsValid)
            {
                _context.Add(eyeControl);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return RedirectToPage("./Index");
        }

        // GET: EyeResult/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eyeControl = await _context.EyeControls.FindAsync(id);
            if (eyeControl == null)
            {
                return NotFound();
            }

            return View(eyeControl);
        }

        // POST: EyeResult/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("EyeControlId,LeftEyeResult,RightEyeResult,Comment,NumberOfTests")]
            EyeControl eyeControl)
        {
            if (id != eyeControl.EyeControlId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(eyeControl);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EyeControlExists(eyeControl.EyeControlId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            return View(eyeControl);
        }

        // GET: EyeResult/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eyeControl = await _context.EyeControls
                .FirstOrDefaultAsync(m => m.EyeControlId == id);
            if (eyeControl == null)
            {
                return NotFound();
            }

            return View(eyeControl);
        }

        // POST: EyeResult/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var eyeControl = await _context.EyeControls.FindAsync(id);
            _context.EyeControls.Remove(eyeControl);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EyeControlExists(int id)
        {
            return _context.EyeControls.Any(e => e.EyeControlId == id);
        }
    }
}