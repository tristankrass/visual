﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.EyeControl;
using Infra.Doctor;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Visual.Areas.Identity.Data;

namespace Visual.Data
{
    public class ApplicationDbContext : IdentityDbContext<VisualUser>
    {
        
        
        public DbSet<EyeControl> EyeControls { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.Entity<VisualUser>(entity =>
            //{
                
            //});
           
            InitializeTables(builder);
            CreateRoles(builder);

        }
        internal void InitializeTables(ModelBuilder builder)
        {
            DoctorDbContext.InitializeTables(builder);
        }

        internal void CreateRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasData(new IdentityRole { Id = "Patient_Id", Name = "Patient", NormalizedName = "PATIENT" });

            builder.Entity<IdentityRole>().HasData(new IdentityRole { Id = "Doctor_Id", Name = "Doctor", NormalizedName = "DOCTOR" });

            builder.Entity<IdentityRole>().HasData(new IdentityRole { Id = "Admin_Id", Name = "Admin", NormalizedName = "ADMIN" });

            builder.Entity<IdentityRole>().HasData(new IdentityRole { Id = "Superuser_Id", Name = "Superuser", NormalizedName = "SUPERUSER" });
        }
    }
}