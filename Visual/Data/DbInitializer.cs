﻿using System;
using System.Linq;
using Data.Doctor;
using Data.Hospital;
using Infra.Doctor;

namespace Visual.Data
{
    public class DbInitializer
    {
        public static void Initialize(DoctorDbContext context)
        {
            context.Database.EnsureCreated();

            // Look for any doctors.
            if (context.Doctor.Any())
            {
                return;   // DB has been seeded
            }

            var doctors = new DoctorsData[]
            {
                
                new DoctorsData(){FirstName="Mari",LastName="Mets",ValidFrom= DateTime.Parse("2019-09-01")},
                new DoctorsData(){FirstName="Mati",LastName="Kala",ValidFrom=DateTime.Parse("2017-09-01")},
                new DoctorsData(){FirstName="Kalle",LastName="Kask",ValidFrom=DateTime.Parse("2018-09-01")},
            };
            foreach (DoctorsData s in doctors)
            {
                context.Doctor.Add(s);
            }
            context.SaveChanges();

            var hospitals = new HospitalData[]
            {
                new HospitalData(){ID= "1050",HospitalName="Silmapolikliinik",Address1= "Rahu" , Address2 = "2", City = "Tallinn"},
                new HospitalData(){ID= "4022",HospitalName="Silmaarst Krista Turman OÜ",Address1= "Ravi", Address2 = "8", City = "Tallinn"},
                new HospitalData(){ID="4041",HospitalName="Taliinna Eriarstikeskus",Address1= "Linnamäe tee", Address2 = "3", City = "Tallinn"},

            };
            foreach (HospitalData c in hospitals)
            {
                context.Hospital.Add(c);
            }
            context.SaveChanges();

          
        }
    }
}
