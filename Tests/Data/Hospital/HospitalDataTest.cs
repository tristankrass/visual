﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Common;
using Data.Doctor;
using Data.Hospital;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Data.Hospital
{
    [TestClass]
    public class HospitalDataTest : SealedClassTests<HospitalData, DefinedEntityData>
    {
        [TestMethod]
        public void HospitalNameTest()
        {
            isNullableProperty(() => obj.HospitalName, x => obj.HospitalName = x);
        }
    }
}
