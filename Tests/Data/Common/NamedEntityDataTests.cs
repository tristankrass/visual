﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Data.Common
{
    [TestClass]
    public class NamedEntityDataTests : AbstractClassTest<NamedEntityData, UniqueEntityData>
    {
        private class testClass : NamedEntityData { }

        [TestInitialize]
        public override void TestInitialize()
        {
            base.TestInitialize();
            obj = new testClass();
        }
        [TestMethod]
        public void EmailTest()
        {
            isNullableProperty(() => obj.Email, x => obj.Email = x);
        }
        [TestMethod]
        public void PhoneNumberTest()
        {
            isNullableProperty(() => obj.PhoneNumber, x => obj.PhoneNumber = x);
        }
        [TestMethod]
        public void IdTest()
        {
            isNullableProperty(() => obj.ID, x => obj.ID = x);
        }

    }
}
