﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit.Sdk;

namespace Tests.Data.Common
{
    [TestClass]
    public class DefinedDataTests : AbstractClassTest<DefinedEntityData, NamedEntityData>
    {
        private class testClass : DefinedEntityData { }

        [TestInitialize]
        public override void TestInitialize()
        {
            base.TestInitialize();
            obj = new testClass();
        }
        [TestMethod]
        public void Address1Test()
        {
            isNullableProperty(() => obj.Address1, x => obj.Address1 = x);
        }
        [TestMethod]
        public void Address2Test()
        {
            isNullableProperty(() => obj.Address2, x => obj.Address2 = x);
        }
        [TestMethod]
        public void Address3Test()
        {
            isNullableProperty(() => obj.Address3, x => obj.Address3 = x);
        }
        [TestMethod]
        public void City()
        {
            isNullableProperty(() => obj.City, x => obj.City = x);
        }
        [TestMethod]
        public void CountyTest()
        {
            isNullableProperty(() => obj.County, x => obj.County = x);
        }
        [TestMethod]
        public void CountryTest()
        {
            isNullableProperty(() => obj.Country, x => obj.Country = x);
        }
        [TestMethod]
        public void ZipCodeTest()
        {
            isNullableProperty(() => obj.ZipCode, x => obj.ZipCode = x);
        }
        [TestMethod]
        public void PhotoTest()
        {
            isNullableProperty(() => obj.Photo, x => obj.Photo = x);
        }
    }
}
