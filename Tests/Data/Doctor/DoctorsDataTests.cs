﻿using Data.Common;
using Data.Doctor;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Data.Doctor
{
    [TestClass]
    public class DoctorsDataTests:SealedClassTests<DoctorsData, DefinedEntityData>
    {
        [TestMethod]
        public void LastNameTest()
        {
            isNullableProperty(() => obj.LastName, x => obj.LastName = x);
        }
        [TestMethod]
        public void FirstNameTest()
        {
            isNullableProperty(() => obj.FirstName, x => obj.FirstName = x);
        }
        [TestMethod]
        public void IdCodeTest()
        {
            isNullableProperty(() => obj.IdCode, x => obj.IdCode = x);
        }
        [TestMethod]
        public void CertificateTest()
        {
            isNullableProperty(() => obj.Certificate, x => obj.Certificate = x);
        }
        [TestMethod]
        public void CertificateDateTest()
        {
            isNullableProperty(() => obj.CertificateDate, x => obj.CertificateDate = x);
        }
    }
}
