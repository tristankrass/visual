﻿using System;
using System.Collections.Generic;
using System.Text;
using Data;
using Data.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Data.Patient
{
    [TestClass]
    public class PatientAndDoctorTests:SealedClassTests<PatientsAndDoctors, UniqueEntityData>
    {
        [TestMethod]
        public void HospitalIdTest()
        {
            isNullableProperty(() => obj.HospitalID, x => obj.HospitalID = x);
        }

        [TestMethod]
        public void DoctorIdTest()
        {
            isNullableProperty(()=>obj.DoctorID, x=>obj.DoctorID=x);
        }
    }
}
