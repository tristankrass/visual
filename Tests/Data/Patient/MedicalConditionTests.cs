﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Common;
using Data.Patient;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Data.Patient
{
    [TestClass]
    public class MedicalConditionTests:SealedClassTests<MedicalConditionData, UniqueEntityData>
    {
        [TestMethod]
        public void CommentTest()
        {
            isNullableProperty(() => obj.Comment, x => obj.Comment = x);
        }
        [TestMethod]
        public void PatientId()
        {
            isNullableProperty(() => obj.PatientId, x => obj.PatientId = x);
        }
    }
}
