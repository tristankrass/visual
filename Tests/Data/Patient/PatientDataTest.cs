﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Common;
using Data.Doctor;
using Data.Patient;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Data.Patient
{
    [TestClass]
    public class PateintDataTest : SealedClassTests<PatientData, NamedEntityData>
    {
        [TestMethod]
        public void LastNameTest()
        {
            isNullableProperty(() => obj.LastName, x => obj.LastName = x);
        }
        [TestMethod]
        public void FirstNameTest()
        {
            isNullableProperty(() => obj.FirstName, x => obj.FirstName = x);
        }
        [TestMethod]
        public void IdCodeTest()
        {
            isNullableProperty(() => obj.IdCode, x => obj.IdCode = x);
        }
        [TestMethod]
        public void GenderTest()
        {
            isNullableProperty(() => obj.Gender, x => obj.Gender = x);
        }
        [TestMethod]
        public void UserIdTest()
        {
            isNullableProperty(() => obj.UserId, x => obj.UserId = x);
        }
    }
}
