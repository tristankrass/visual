﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Facade.Doctor;

namespace Facade.Hospital
{
    public class HospitalView
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public string HospitalName { get; set; }
        public string Location { get; set; }
        public long PhoneNumber { get; set; }
        public string Email { get; set; }
        public ICollection<Enrollment> Enrollments { get; set; }
    }
}
