﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Facade.Doctor
{
    public class DoctorView
    {
        public string ID { get; set; }
        public string Certificate { get; set; }
        public DateTime CertificateDate { get; set; }
        public string LastName { get; set; }
        public string FirstMidName { get; set; }
        public string IdCode { get; set; }
        public string Location { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Photo { get; set; }
        public DateTime EnrollmentDate { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }
    }
}