﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Hospital;
using Facade.Hospital;

namespace Facade.Doctor
{
    public class Enrollment
    {
        public string EnrollmentID { get; set; }
        public string HospitalID { get; set; }
        public string DoctorID { get; set; }
       
      //  public HospitalView Hospital { get; set; }
      // public DoctorView Doctor { get; set; }
    }
}
