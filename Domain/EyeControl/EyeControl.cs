using System.ComponentModel.DataAnnotations;

namespace Domain.EyeControl
{
    public class EyeControl
    {
        public int EyeControlId { get; set; }

        [Required, MinLength(2), MaxLength(2048)]
        public string LeftEyeResult { get; set; } = default!;
 
        [Required, MinLength(2), MaxLength(2048)]
        public string RightEyeResult { get; set; } = default!;
        
        
        [MinLength(2), MaxLength(128)]public string? Comment { get; set; }

        public int NumberOfTests { get; set; } = 4;

    }
}