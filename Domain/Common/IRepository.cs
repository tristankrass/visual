﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface  IRepository<T>
    {
        Task<List<T>> Get();
        Task<T> Get(string id);
        Task Delete(string id);
        Task Add(T obj);
        Task Update(T obj);
        string SortOrder { get; set; }
        string SearchingString { get; set; }
        string PageIndex { get; set; }
        string HasNextPage { get; set; }
        string HasPrevioysPage { get; set; }

    }
}
