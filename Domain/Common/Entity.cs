﻿using Data.Common;

namespace Domain.Doctor
{
    public abstract class Entity<T> where  T: PeriodData
    {
        public T Data { get; set; }

        protected Entity(T data)
        {
            Data = data;
        }
    }
}