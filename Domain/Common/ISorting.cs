﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Common
{
    public interface ISorting
    {
        string SortOrder { get; set; }
    }
}
