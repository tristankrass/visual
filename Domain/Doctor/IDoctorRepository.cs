﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Domain.Doctor
{
    public interface IDoctorRepository : IRepository<Doctor>
    {
    }
}
