﻿using Data.Doctor;


namespace Domain.Doctor
{
    public class Doctor : Entity<DoctorsData>
    {
        public Doctor() : this(null) { }
        public Doctor(DoctorsData data = null) : base(data)
        {
        }

    }
}
