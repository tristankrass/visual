﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Hospital
{
    public interface IHospitalRepository : IRepository<Hospital>
    {
    }
}
