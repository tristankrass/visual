﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Data.Doctor;
using Data.Hospital;
using Domain.Doctor;

namespace Domain.Hospital
{
    public class Hospital : Entity<HospitalData>
    {
        public Hospital() : this(null) { }
        public Hospital(HospitalData data=null) : base(data)
        {
        }
    }
}
